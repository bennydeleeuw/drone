//Parts of this code are based on the code of Joop Brokking.

int value = 0; // set values you need to zero

unsigned long loop_timer, esc_loop_timer;
unsigned long timer_esc_1[4];
int esc_1[4];


// Keep the loop freq. on 250hz.
// 1 / 250 = 0.004 sec = 4 ms = 4000 us

void setup() {

  //  esc1.attach(4);    // attached to pin 9 I just do this with 1 Servo
  Serial.begin(9600);    // start serial at 9600 baud


  DDRD |= B11110000; //Configure digital poort  4, 5, 6 and 7 as output.

  for (int cal_int = 0; cal_int < 1250; cal_int++)
  { //Wait 5 seconds before continuing.
    PORTD |= B11110000;      //Set digital port 4, 5, 6 and 7 high.
    delayMicroseconds(1000); //Wait 1000us.
    PORTD &= B00001111;      //Set digital port 4, 5, 6 and 7 low.
    delayMicroseconds(3000); //Wait 3000us.
  }

  loop_timer = micros();

}



void loop() {

  esc_1[0] = 1100;
  esc_1[1] = 1100;
  esc_1[2] = 1100;
  esc_1[3] = 1100;


  if (micros() - loop_timer > 4050) {
    //ERROR! Frequency below 250hz!
  }

  while (micros() - loop_timer < 4000);

  loop_timer = micros();

  PORTD |= B11110000;      //Set digital port 4, 5, 6 and 7 high.

  timer_esc_1[00] = esc_1[00] + loop_timer;
  timer_esc_1[01] = esc_1[01] + loop_timer;
  timer_esc_1[02] = esc_1[02] + loop_timer;
  timer_esc_1[03] = esc_1[03] + loop_timer;

  while (PORTD >= 16) {

    esc_loop_timer = micros(); //Read the current time.

    if (timer_esc_1[0] <= esc_loop_timer)
      PORTD &= B11100000;      //Set digital port 4 low.
    if (timer_esc_1[1] <= esc_loop_timer)
      PORTD &= B11010000;      //Set digital port 5 low.
    if (timer_esc_1[2] <= esc_loop_timer)
      PORTD &= B10110000;      //Set digital port 6 low.
    if (timer_esc_1[3] <= esc_loop_timer)
      PORTD &= B01110000;      //Set digital port 7 low.
  }





}
